import tc_etl_lib as tc

from config.config import DEVICE_API_KEY, DEVICE_MEASURE_MODEL


def setup_iot_manager(sensor_id):
    """
    Sets up and returns an IoT manager instance
    configured for the given sensor ID.
    """
    return tc.iota.iotaManager(
        endpoint=DEVICE_MEASURE_MODEL,
        api_key=DEVICE_API_KEY,
        sensor_id=sensor_id
    )


def main_loader(ready_to_load_data, log):
    """
    Processes the data loading phase, including sending
    data to the IoT platform, and logs the progress.
    This function orchestrates the setting up
    of an IoT manager for each entry,
    and sends the data.

    Parameters:
    ready_to_load_data (dict): A dictionary where keys are neighborhood names
    and values are lists of data dictionaries to be sent.
    log (Logger): Logger instance for logging messages.

    Returns:
    None
    """
    log.logger.info("Loading phase started...")
    for neighborhood, measurements in ready_to_load_data.items():
        for data in measurements:
            sensor_id = data.get('sensorId', 'default_sensor_id')

            # Setup the IoT manager for the sensor
            iot_manager = setup_iot_manager(sensor_id)
            data.pop('sensorId', None)
            iot_manager.send_http(data=data)
            log.logger.info(
                f"Data {data} sent to IoT for sensor {sensor_id} "
                f"in {neighborhood}"
            )
    log.logger.info("Loading phase completed.")
