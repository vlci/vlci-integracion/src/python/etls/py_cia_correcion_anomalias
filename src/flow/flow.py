import logging
import sys
import traceback

FAILED_EXEC = 1
SUCCESS_EXEC = 0


class FlowControl:
    '''Class that implements methods used to control the ETL flow,
    for example, to terminate execution, allow continuation but send an email
    at the end, or indicate that execution was successful.'''

    def __init__(self, log: logging.Logger) -> None:
        self.flow_state = SUCCESS_EXEC
        self.log = log

    def handle_error(self, cause: str, fatal: bool = False) -> None:
        '''Logs the received error, adds it to the email to be sent, and
        if it is a fatal error, terminates the ETL execution.'''
        self.flow_state = FAILED_EXEC

        if fatal:
            traceback_str = traceback.format_exc()
            self.log.error(traceback_str)
            self.log.error(cause)
            self.end_exec()
        else:
            self.log.error("Validation error.Details can be seen in the logs.")

    def end_exec(self) -> None:
        '''Terminates the ETL execution returning 1 in case of failure and 0
        in case of success.'''

        if self.flow_state == FAILED_EXEC:
            self.log.info('Execution Failed, ETL KO')
        else:
            self.log.info('Execution Successful, ETL OK')

        sys.exit(self.flow_state)
