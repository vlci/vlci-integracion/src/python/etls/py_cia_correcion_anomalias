import pandas as pd

from config.config import (DEFAULT_DATE, QUERY_LAST_CORRECTION_DATE,
                           QUERY_LAST_MESURE_DATE)
from config.entities_mappings import (description_mapping, name_mapping,
                                      sensor_id_mapping)
from extractor import read_from_db


def reindex_data(raw_data_df, corrected_data, neighborhood):
    """
    Reindexes corrected data to match the original data
    indices based on the specified neighborhood.
    """
    original_data = raw_data_df[raw_data_df['neighborhood'] == neighborhood]
    corrected_data_reindexed = corrected_data.reindex(
        original_data.index, method='nearest')
    return original_data, corrected_data_reindexed


def combine_data(original_data, corrected_data_reindexed):
    """
    Combines the original and corrected data into a single DataFrame.
    """
    return pd.concat([original_data, corrected_data_reindexed[
        ['Corrected_Data_Final']]],
        axis=1
    )


def prepare_measurements(combined_data, neighborhood, start_date, end_date):
    """
    Prepares and returns measurement data as a list of dictionaries for
    the specified neighborhood and date range.
    """
    filtered_combined_data = combined_data.loc[start_date:end_date]
    measurements = []

    for index, row in filtered_combined_data.iterrows():
        measurement = {
            "type": "KeyPerformanceIndicator",
            "calculationPeriod": index.strftime('%Y-%m-%d'),
            "description": description_mapping.get(neighborhood),
            "isAggregated": "N",
            "kpiValueOriginal": row['original_kpivalue'],
            "name": row['name'],
            "neighborhood": neighborhood,
            "dayofweek": row['dayofweek'],
            "kpiValue": row['Corrected_Data_Final'],
            "operationalStatus": "ok",
            "sensorId": sensor_id_mapping.get(row['name'])
        }
        measurements.append(measurement)

    return measurements


def calculate_offset(
        last_corrected_values,
        last_measured_values,
        default_date):
    """
    Updates the offset_data DataFrame with offset, start_date,
    and end_date for data fetching.

    Args:
        last_corrected_values (pd.DataFrame): DataFrame containing
        the last_correction_date column.
        last_measured_values (pd.DataFrame): DataFrame containing
        the last_measured_date column.
        default_date (str): The default date to use
        if last_correction_date is undefined. Default is '2023-01-01 00:00:00'.

    Returns:
        pd.DataFrame: Updated DataFrame with offset,
        start_date, and end_date columns.
    """
    # Convert default_date to datetime
    default_date = pd.to_datetime(default_date)

    # Handle empty last_corrected_values
    if last_corrected_values.empty:
        last_corrected_values = pd.DataFrame({
            'neighborhood': last_measured_values['neighborhood'],
            'last_correction_date': [default_date] * len(last_measured_values)
        })

    # Merge the corrected and measured values
    offset_data = pd.merge(
        last_corrected_values,
        last_measured_values,
        on='neighborhood',
        suffixes=('_correction', '_measure')
    )

    # Rename the columns to ensure they have the correct suffixes
    offset_data.rename(columns={
        'last_correction_date': 'last_correction_date_correction',
        'last_measure_date': 'last_measure_date'
    }, inplace=True)

    # Convert dates to datetime
    offset_data['last_correction_date_correction'] = pd.to_datetime(
        offset_data['last_correction_date_correction'],
        errors='coerce').fillna(default_date)
    offset_data['last_measure_date'] = pd.to_datetime(
        offset_data['last_measure_date'], errors='coerce')

    # Calculate the gap and offset ensuring a one-week gap
    # ensuring that the last corrected date is not corrected again
    offset_data['gap_days'] = (offset_data['last_measure_date'] -
                               offset_data['last_correction_date_correction']
                               ).dt.days
    offset_data['offset'] = offset_data['gap_days'] - 7
    offset_data['offset'] = offset_data['offset'].apply(
        lambda x: max(x, 0))  # Ensure no negative offset

    # Calculate start and end dates only if offset > 0
    offset_data['start_date'] = offset_data[
        'last_correction_date_correction'] + \
        pd.Timedelta(days=1)
    offset_data['end_date'] = offset_data['start_date'] + \
        pd.to_timedelta(offset_data['offset'], unit='d')

    # If offset is 0, set start_date and end_date to NaT (Not a Time)
    offset_data.loc[offset_data['offset'] ==
                    0, ['start_date', 'end_date']] = pd.NaT

    return offset_data


def main_postprocess(all_neighborhood_dataframes, raw_data_df, log):
    """
    Main function to post-process the data for all neighborhoods.

    Args:
        all_neighborhood_dataframes (dict): Dictionary of DataFrames 
        with corrected data for each neighborhood.
        raw_data_df (pd.DataFrame): Raw data DataFrame.
        log (logging.Logger): Logger for logging information.

    Returns:
        dict: Dictionary with the final data ready for sending, 
        organized by neighborhood.
    """
    log.logger.info("Data postprocessing started...")
    default_date = DEFAULT_DATE
    final_data_for_sending = {}

    # Read last correction and measurement dates from the database
    last_corrected_values = read_from_db(QUERY_LAST_CORRECTION_DATE)
    last_measured_values = read_from_db(QUERY_LAST_MESURE_DATE)

    # Calculate the offsets
    offset_data = calculate_offset(
        last_corrected_values, last_measured_values, default_date)

    offset_data['neighborhood'] = offset_data['neighborhood'].map(
        name_mapping).fillna(offset_data['neighborhood'])

    for neighborhood, corrected_data in all_neighborhood_dataframes.items():
        neighborhood_offsets = offset_data[
            offset_data['neighborhood'].str.strip(
            ).str.lower() == neighborhood.lower().strip()]

        # Skip processing if neighborhood_offsets is empty
        if neighborhood_offsets.empty or neighborhood_offsets[
                'offset'].values[0] == 0:
            continue

        original_data, corrected_data_reindexed = reindex_data(
            raw_data_df, corrected_data, neighborhood)
        combined_data = combine_data(original_data, corrected_data_reindexed)
        measurements_list = []

        for _, offset_row in neighborhood_offsets.iterrows():
            start_date = offset_row['start_date']
            end_date = offset_row['end_date']

            # Skip processing if start_date or end_date is NaT
            if pd.isna(start_date) or pd.isna(end_date):
                continue

            measurements = prepare_measurements(
                combined_data, neighborhood, start_date, end_date)
            measurements_list.extend(measurements)

        if measurements_list:
            final_data_for_sending[neighborhood] = measurements_list

    log.logger.info("Data postprocessing completed.")
    return final_data_for_sending
