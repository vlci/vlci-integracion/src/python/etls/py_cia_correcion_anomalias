import pandas as pd


def initialize_columns(df):
    """
    Ensures that necessary columns are present 
    and correctly formatted in the DataFrame.
    """
    if 'dayofweek' not in df.columns:
        df['dayofweek'] = df.index.dayofweek + 1
    if 'season' not in df.columns:
        df['season'] = df.index.to_series().apply(assign_season)
    if 'Corrected_Data_Conditional' not in df.columns:
        df['Corrected_Data_Conditional'] = df['Original_Value']


def assign_season(date):
    """
    Assigns a season to a given date.
    """
    year = date.year
    seasons = {
        'spring': (pd.Timestamp(year=year, month=3, day=21),
                   pd.Timestamp(year=year, month=6, day=20)),
        'summer': (pd.Timestamp(year=year, month=6, day=21),
                   pd.Timestamp(year=year, month=9, day=22)),
        'fall': (pd.Timestamp(year=year, month=9, day=23),
                 pd.Timestamp(year=year, month=12, day=20)),
        'winter': (pd.Timestamp(year=year, month=12, day=21),
                   pd.Timestamp(year=year + 1, month=3, day=20))
    }

    for season, (start, end) in seasons.items():
        if start <= date <= end:
            return season
    # If date falls in the winter period of the next year
    return 'winter'


def calculate_weekday_medians(df):
    """
    Calculates and returns the mean values for each weekday 
    from non-anomalous data, separated by season.
    """
    medians = df.loc[~df['Confirmed_Anomaly']].groupby(
        ['season', 'dayofweek']
    )['Original_Value'].median()
    return medians


def correct_single_anomaly(df, index, row, weekday_means):
    """
    Corrects a single anomalous data point using Conditional Mean Imputation,
    and stores the corrected value as the final corrected value.
    """
    season = row['season']
    day_of_week = row['dayofweek']

    # Correct using Conditional Mean Imputation
    if (season, day_of_week) in weekday_means:
        corrected_value_conditional = weekday_means[(season, day_of_week)]
    else:
        # Fallback if no mean data available
        corrected_value_conditional = row['Original_Value']
    corrected_value_conditional = round(corrected_value_conditional, 2)

    # Update DataFrame with the corrected value
    df.at[index, 'Corrected_Data_Conditional'] = corrected_value_conditional
    df.at[index, 'Corrected_Data_Final'] = corrected_value_conditional


def correct_all_anomalies(df):
    """
    Orchestrates the correction of anomalies in all 
    data rows of a DataFrame using Conditional Mean Imputation.
    """
    initialize_columns(df)
    weekday_means = calculate_weekday_medians(df)
    for index, row in df.iterrows():
        if row['Confirmed_Anomaly']:
            correct_single_anomaly(df, index, row, weekday_means)
        else:
            df.at[index, 'Corrected_Data_Conditional'] = row['Original_Value']
            df.at[index, 'Corrected_Data_Final'] = row['Original_Value']
    return df


def main_corrector(all_neighborhood_dataframes, log):
    """
    Applies anomaly correction to all dataframes within the given collection,
    mapped by neighborhood.
    """
    log.logger.info("Anomaly correction started.")

    corrected_dataframes = {}
    for neighborhood, df in all_neighborhood_dataframes.items():
        corrected_df = correct_all_anomalies(df)
        corrected_dataframes[neighborhood] = corrected_df
    log.logger.info("Anomaly correction complete.")

    return corrected_dataframes
