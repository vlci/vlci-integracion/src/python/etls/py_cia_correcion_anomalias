import numpy as np
from sklearn.neighbors import NearestNeighbors
from sklearn.preprocessing import RobustScaler


def detect_anomalies_with_knn(time_series_data, best_params):
    data = time_series_data.copy()
    data = data.asfreq('D', method='pad')
    scaler = RobustScaler()
    kpivalue_scaled = scaler.fit_transform(
        data[
            ['original_kpivalue']
        ].values.reshape(-1, 1))

    knn = NearestNeighbors(**best_params['KNN'])
    knn.fit(kpivalue_scaled)
    distances, indices = knn.kneighbors(kpivalue_scaled)
    kth_distances = distances[:, knn.n_neighbors - 1]
    threshold = 15 * np.mean(kth_distances)
    data['Anomaly_KNN'] = kth_distances > threshold
    return data
