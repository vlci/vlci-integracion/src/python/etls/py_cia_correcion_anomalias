from sklearn.preprocessing import RobustScaler
from sklearn.svm import OneClassSVM


def detect_anomalies_with_svm(time_series_data, best_params):
    data = time_series_data.copy()
    scaler = RobustScaler()
    kpivalue_scaled = scaler.fit_transform(data[['original_kpivalue']])

    oc_svm = OneClassSVM(**best_params['SVM'])
    oc_svm.fit(kpivalue_scaled)
    scores = oc_svm.decision_function(kpivalue_scaled)  # Confidence scores
    data['svm_pred'] = oc_svm.predict(kpivalue_scaled)
    data['Anomaly_SVM'] = data['svm_pred'] == -1
    data['Confidence_SVM'] = scores
    return data
