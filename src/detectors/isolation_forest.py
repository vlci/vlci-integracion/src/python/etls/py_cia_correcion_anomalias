from sklearn.ensemble import IsolationForest
from sklearn.preprocessing import RobustScaler


def detect_with_isolation_forest(time_series_data, best_params):
    data = time_series_data.copy()
    scaler = RobustScaler()
    kpivalue_scaled = scaler.fit_transform(data[['original_kpivalue']])

    isolation_forest = IsolationForest(**best_params['IsolationForest'])
    isolation_forest.fit(kpivalue_scaled)
    scores = isolation_forest.decision_function(kpivalue_scaled)
    data['Anomaly_IsolationForest'] = isolation_forest.predict(
        kpivalue_scaled) == -1
    data['Confidence_IsolationForest'] = scores
    return data
