
from sklearn.neighbors import LocalOutlierFactor
from sklearn.preprocessing import RobustScaler


def detect_anomalies_with_lof(time_series_data, best_params):
    data = time_series_data.copy()
    scaler = RobustScaler()
    kpivalue_scaled = scaler.fit_transform(data[['original_kpivalue']])

    lof = LocalOutlierFactor(**best_params['LOF'])
    lof.fit_predict(kpivalue_scaled)
    scores = -lof.negative_outlier_factor_  # Inverse of LOF scores
    data['Anomaly_LOF'] = lof.fit_predict(kpivalue_scaled) == -1
    data['Confidence_LOF'] = scores
    return data
