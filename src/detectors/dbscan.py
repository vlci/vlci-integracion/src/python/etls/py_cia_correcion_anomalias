from sklearn.cluster import DBSCAN
from sklearn.preprocessing import RobustScaler


def detect_anomalies_with_dbscan(time_series_data, best_params):
    data = time_series_data.copy()
    scaler = RobustScaler()
    kpivalue_scaled = scaler.fit_transform(data[['original_kpivalue']])

    dbscan = DBSCAN(**best_params['DBSCAN'])
    clusters = dbscan.fit_predict(kpivalue_scaled)
    data['Anomaly_DBSCAN'] = clusters == -1
    return data
