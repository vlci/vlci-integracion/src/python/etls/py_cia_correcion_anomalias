import pandas as pd

from config.config import THRESHOLD
from config.models_params import PARAMS_DICT, WEIGHTS
from detectors.dbscan import detect_anomalies_with_dbscan
from detectors.isolation_forest import detect_with_isolation_forest
from detectors.knn import detect_anomalies_with_knn
from detectors.lof import detect_anomalies_with_lof
from detectors.svm import detect_anomalies_with_svm
from detectors.z_score import detect_anomalies_with_zscore


# Combine anomalies from different methods
def combine_anomalies(time_series_data, anomalies_results):
    anomalies_df_combined = pd.DataFrame(
        {'Original_Value': time_series_data['original_kpivalue']},
        index=time_series_data.index
    )
    for method, result in anomalies_results.items():
        anomalies_df_combined[f'Anomaly_{method}'] = result[
            f'Anomaly_{method}']
    return anomalies_df_combined


# Calculate the anomaly score based on specific weights
def calculate_anomaly_score(anomalies_df_combined, weights):
    for method, weight in weights.items():
        column_name = f'Anomaly_{method}'
        if column_name in anomalies_df_combined.columns:
            anomalies_df_combined[column_name] = (
                anomalies_df_combined[column_name].astype(int) * weight
            )
        else:
            anomalies_df_combined[column_name] = 0
    anomalies_df_combined['Anomaly_Score'] = anomalies_df_combined[
        [f'Anomaly_{method}' for method in weights]
    ].sum(axis=1)
    return anomalies_df_combined


# Confirm anomalies based on the calculated score
def confirm_anomalies(anomalies_df_combined):
    anomalies_df_combined['Confirmed_Anomaly'] = (
        anomalies_df_combined['Anomaly_Score'] > THRESHOLD
    )
    return anomalies_df_combined


def ensure_numeric(df):
    df['original_kpivalue'] = pd.to_numeric(
        df['original_kpivalue'], errors='coerce'
    )
    df = df.dropna(subset=['original_kpivalue'])
    return df


def main_detector(neighborhood_dataframes, log):
    log.logger.info("Anomaly detection started...")
    all_anomalies = {}
    neighborhood_weights = WEIGHTS
    best_params_dict = PARAMS_DICT

    for neighborhood, df in neighborhood_dataframes.items():
        log.logger.info(f"Processing neighborhood: {neighborhood}")
        data = ensure_numeric(df)
        best_params = best_params_dict.get(neighborhood, {})
        weights = neighborhood_weights.get(neighborhood, {})

        anomaly_methods = {
            'KNN': lambda data: detect_anomalies_with_knn(data, best_params),
            'SVM': lambda data: detect_anomalies_with_svm(data, best_params),
            'Zscore': detect_anomalies_with_zscore,
            'DBSCAN': lambda data: detect_anomalies_with_dbscan(
                data, best_params),
            'IsolationForest': lambda data: detect_with_isolation_forest(
                data, best_params
            ),
            'LOF': lambda data: detect_anomalies_with_lof(data, best_params)
        }

        anomalies_results = {
            method: func(data) for method, func in anomaly_methods.items()
        }
        anomalies_df_combined = combine_anomalies(data, anomalies_results)
        anomalies_df_combined = calculate_anomaly_score(
            anomalies_df_combined,
            weights)
        anomalies_df_combined = confirm_anomalies(anomalies_df_combined)
        all_anomalies[neighborhood] = anomalies_df_combined

    log.logger.info("Anomaly detection completed.")
    return all_anomalies
