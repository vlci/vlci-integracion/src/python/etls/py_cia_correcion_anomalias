def detect_anomalies_with_zscore(time_series_data):
    data = time_series_data.copy()

    # Calculate z-score
    data['z_score'] = (data['original_kpivalue'] -
                       data['original_kpivalue'].mean()
                       ) / data['original_kpivalue'].std()

    # Set threshold for anomaly detection
    threshold = 2

    # Mark anomalies based on z-score
    data['Anomaly_Zscore'] = abs(data['z_score']) > threshold

    return data
