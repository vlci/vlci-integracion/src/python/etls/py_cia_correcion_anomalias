import pandas as pd
from sqlalchemy import create_engine

from config.config import (DB_NAME, DB_PASSWORD, DB_USERNAME, HOST, PORT,
                           QUERY_EXTRACT_DATA, SCHEMA)


def read_from_db(query):
    """
    Read data from database using SQL query and return as DataFrame.

    :param secrets: Dictionary containing database connection info.
    :param query: SQL query as a string.
    :return: DataFrame containing the query results.
    """
    list_chunks = []
    # Create the connection string
    connection_string = (
        f"postgresql://{DB_USERNAME}:{DB_PASSWORD}"
        f"@{HOST}:{PORT}/{DB_NAME}"
    )
    # Create the SQL Alchemy engine
    engine = create_engine(
        connection_string,
        connect_args={'options': f"-csearch_path={SCHEMA}"}
    )

    # Read data from database in chunks and process each chunk
    for chunk in pd.read_sql_query(query, engine, chunksize=2000):
        list_chunks.append(chunk)

    return pd.concat(list_chunks)


def main_extractor(log):
    """
    Main function to execute module logic
    """
    log.logger.info("Data extraction started...")
    raw_data_df = read_from_db(QUERY_EXTRACT_DATA)
    log.logger.info("Data extraction completed.")
    return raw_data_df
