import os

# Configuración ETL
EXEC_ENV = os.getenv('entorno_ejecucion', None)
ETL_NAME = os.getenv('etl_name', 'PY_CIA_CORRECION_ANOMALIAS_INYECCION_AGUA')

# Configuración logs
LOGGING_LEVEL = str(os.getenv('loging_level', 'INFO'))
LOGGING_FOLDER = str(os.getenv('loging_folder', 'logs'))

# Configuración IoT Agent
IOT_JSON_AGENT_URL = os.getenv('iot_json_agent_url', None)
IOT_SLEEP_INSERT_MILI = int(os.getenv('iot_sleep_insert_mili', 250))

# Configuración Device
DEVICE_API_KEY = os.getenv('device_api_key', None)
DEVICE_MEASURE_MODEL = os.getenv('device_measure_model', None)

# Configuración Queries
QUERY_EXTRACT_DATA = os.getenv('query_extract_data', None)
QUERY_LAST_MESURE_DATE = os.getenv('query_last_mesure_date', None)
QUERY_LAST_CORRECTION_DATE = os.getenv('query_last_correction_date', None)

DEFAULT_DATE = os.getenv('default_date', '2020-03-05 00:00:00')

# Configuración base de datos
DB_USERNAME = os.getenv('global_database_postgis_login_vlci2', None)
DB_PASSWORD = os.getenv('global_database_postgis_password_vlci2', None)
HOST = os.getenv('global_database_postgis_host_vlci2', None)
PORT = os.getenv('global_database_postgis_port_vlci2', None)
DB_NAME = os.getenv('global_database_postgis_database_vlci2', None)
SCHEMA = os.getenv('global_database_postgis_schema_vlci2', None)

# Configuración parametros de modelos
THRESHOLD = float(os.getenv('threshold', 0.4))
