WEIGHTS = {
    'Benimamet': {
        'KNN': 0.0,
        'SVM': 0.0,
        'Zscore': 0.10,
        'DBSCAN': 0.65,
        'IsolationForest': 0.16,
        'LOF': 0.10
    },
    'Exposicio': {
        'KNN': 0.18,
        'SVM': 0.26,
        'Zscore': 0.10,
        'DBSCAN': 0.21,
        'IsolationForest': 0.19,
        'LOF': 0.07
    },
    'Malilla': {
        'KNN': 0.18,
        'SVM': 0.11,
        'Zscore': 0.20,
        'DBSCAN': 0.12,
        'IsolationForest': 0.26,
        'LOF': 0.13
    },
    'Mestalla': {
        'KNN': 0.05,
        'SVM': 0.14,
        'Zscore': 0.16,
        'DBSCAN': 0.28,
        'IsolationForest': 0.27,
        'LOF': 0.10
    },
    'Perellonet': {
        'KNN': 0.0,
        'SVM': 0.0,
        'Zscore': 0.0,
        'DBSCAN': 1.0,
        'IsolationForest': 0.0,
        'LOF': 0.0
    },
    'Soternes': {
        'KNN': 0.0,
        'SVM': 0.0,
        'Zscore': 0.07,
        'DBSCAN': 0.74,
        'IsolationForest': 0.12,
        'LOF': 0.07
    },
    'Valencia ciudad': {
        'KNN': 0.14,
        'SVM': 0.08,
        'Zscore': 0.26,
        'DBSCAN': 0.27,
        'IsolationForest': 0.15,
        'LOF': 0.10
    }
}

PARAMS_DICT = {
    'Benimamet': {
        'SVM': {
            'nu': 0.001,
            'kernel': 'rbf',
            'gamma': 0.1
        },
        'KNN': {
            'n_neighbors': 2
        },
        'IsolationForest': {
            'n_estimators': 400,
            'max_samples': 1.0,
            'contamination': 0.04
        },
        'DBSCAN': {
            'min_samples': 14,
            'eps': 0.70
        },
        'LOF': {
            'n_neighbors': 37,
            'contamination': 0.07
        }
    },
    'Exposicio': {
        'SVM': {
            'nu': 0.01,
            'kernel': 'rbf',
            'gamma': 0.1
        },
        'KNN': {
            'n_neighbors': 2
        },
        'IsolationForest': {
            'n_estimators': 500,
            'max_samples': 0.25,
            'contamination': 0.02
        },
        'DBSCAN': {
            'min_samples': 14,
            'eps': 0.70
        },
        'LOF': {
            'n_neighbors': 37,
            'contamination': 0.07
        }
    },
    'Malilla': {
        'SVM': {
            'nu': 0.1,
            'kernel': 'rbf',
            'gamma': 1.0
        },
        'KNN': {
            'n_neighbors': 2
        },
        'IsolationForest': {
            'n_estimators': 100,
            'max_samples': 1.0,
            'contamination': 0.02
        },
        'DBSCAN': {
            'min_samples': 14,
            'eps': 0.70
        },
        'LOF': {
            'n_neighbors': 37,
            'contamination': 0.07
        }
    },
    'Mestalla': {
        'SVM': {
            'nu': 0.01,
            'kernel': 'rbf',
            'gamma': 0.1
        },
        'KNN': {
            'n_neighbors': 2
        },
        'IsolationForest': {
            'n_estimators': 500,
            'max_samples': 0.5,
            'contamination': 0.02
        },
        'DBSCAN': {
            'min_samples': 14,
            'eps': 0.70
        },
        'LOF': {
            'n_neighbors': 37,
            'contamination': 0.07
        }
    },
    'Perellonet': {
        'SVM': {
            'nu': 0.01,
            'kernel': 'rbf',
            'gamma': 1.0
        },
        'KNN': {
            'n_neighbors': 2
        },
        'IsolationForest': {
            'n_estimators': 200,
            'max_samples': 1.0,
            'contamination': 0.07
        },
        'DBSCAN': {
            'min_samples': 14,
            'eps': 0.70
        },
        'LOF': {
            'n_neighbors': 37,
            'contamination': 0.07
        }
    },
    'Soternes': {
        'SVM': {
            'nu': 0.001,
            'kernel': 'rbf',
            'gamma': 0.1
        },
        'KNN': {
            'n_neighbors': 2
        },
        'IsolationForest': {
            'n_estimators': 200,
            'max_samples': 'auto',
            'contamination': 0.04
        },
        'DBSCAN': {
            'min_samples': 14,
            'eps': 0.70
        },
        'LOF': {
            'n_neighbors': 37,
            'contamination': 0.07
        }
    },
    'Valencia ciudad': {
        'SVM': {
            'nu': 0.1,
            'kernel': 'rbf',
            'gamma': 0.01
        },
        'KNN': {
            'n_neighbors': 2
        },
        'IsolationForest': {
            'n_estimators': 400,
            'max_samples': 'auto',
            'contamination': 0.04
        },
        'DBSCAN': {
            'min_samples': 14,
            'eps': 0.70
        },
        'LOF': {
            'n_neighbors': 37,
            'contamination': 0.07
        }
    }
}
