sensor_id_mapping = {
    "Consumo_agua_total_exposicio": "kpicorregido-exposicio-total",
    "Consumo_agua_total_malilla": "kpicorregido-malilla-total",
    "Consumo_agua_total_perellonet": "kpicorregido-perellonet-total",
    "Consumo_agua_total_soternes": "kpicorregido-soternes-total",
    "Consumo_agua_total_mestalla": "kpicorregido-mestalla-total",
    "Consumo_agua_total_benimamet": "kpicorregido-benimamet-total",
    "Consumo_agua_total_valencia": "kpicorregido-valencia-ciudad-total"
}
description_mapping = {
    "Exposicio": "Caudal de agua inyectado en el barrio de Exposicio.",
    "Malilla": "Caudal de agua inyectado en el barrio de Malilla.",
    "Perellonet": "Caudal de agua inyectado en el barrio de Perellonet.",
    "Soternes": "Caudal de agua inyectado en el barrio de Soternes.",
    "Mestalla": "Caudal de agua inyectado en el barrio de Mestalla.",
    "Benimamet": "Caudal de agua inyectado en el barrio de Benimamet.",
    "Valencia ciudad": "Cantidad de agua inyectada en la ciudad de Valencia."
}
name_mapping = {
    'valencia': 'Valencia ciudad'
}
