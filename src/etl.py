from config import config
from corrector import main_corrector
from data_processors.postprocessor import main_postprocess
from data_processors.preprocessor import main_preprocessor
from detectors.detector import main_detector
from extractor import main_extractor
from flow.flow import FlowControl
from loader import main_loader
from logger.logger import Logger


def main():

    # Initialization
    log = Logger(
        config.LOGGING_FOLDER,
        config.ETL_NAME,
        config.LOGGING_LEVEL
    )
    flow = FlowControl(log)

    # ETL main jobs
    try:
        log.logger.info("Starting ETL process...")

        raw_data_df = main_extractor(log)
        neighborhood_dataframes = main_preprocessor(
            raw_data_df,
            log
        )
        dataframes_with_flagged_anomalies = main_detector(
            neighborhood_dataframes,
            log
        )
        corrected_dataframes = main_corrector(
            dataframes_with_flagged_anomalies,
            log
        )
        ready_to_load_data = main_postprocess(
            corrected_dataframes,
            raw_data_df,
            log
        )
        main_loader(ready_to_load_data, log)
        return flow.end_exec()

    except Exception as e:
        flow.handle_error(
            cause=f'Error en el main proceso de la ETL: {e}',
            fatal=True)


if __name__ == "__main__":
    main()
